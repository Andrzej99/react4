import './styles.css';
import { useContext, useEffect } from 'react';
import { CharsStats } from './../../context'
import TableRow from './../TableRow';
/* Odbieranie dzieci komponentu User*/


const User = () => {
    const { name, str, hp, speed,addStr,addName,dmg,lvl } = useContext(CharsStats);
    const myStats={name,str,hp,speed,dmg,lvl};

    useEffect(()=>{
        let nam=localStorage.getItem('name');
        addName(nam);
//        localStorage.removeItem('name');
    })
    

    return (
        <div className="col-6">
            <TableRow {...myStats}/>
 
        </div>
    )


}


export default User;