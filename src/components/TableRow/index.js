import React, { useContext, useEffect } from 'react';
import './TableRow.css';
import { GameState, CharsStats, CommonStatsProvider } from './../../context';



const TableRow = props => {
    const { name, str, hp, speed, addStr, addName, type, changeType } = useContext(CharsStats);


    return (
        <>
            <table>
                <tbody>
                    <tr><td>Nick: {props.name}</td></tr>
                    <tr><td>Sila: {props.str}</td></tr>
                    <tr><td>Życie: {props.hp}</td></tr>
                    <tr><td>Prędkość atak.: {props.speed}</td></tr>
                    <tr><td>Obrażenia: {props.dmg}</td></tr>
                </tbody>
            </table>
            <div>
                 {type==='User' ? ( 
                
                    <div className="result-wrapper"> {/*display flex */}
                        <div className="result-wrapper__text-description"> {/*flex basis 20% opis tekstowy */}
                            <p className="result-wrapper__paragraf">Poziom: 1</p>
                        </div>
                        <div className="result-wrapper__progress-background">{/*flex basis 70% */}
                            <div className="result-wrapper__progress"></div>
                        </div>
                    </div>
                ) : null 
               }
            </div>
        </>

    )
}

export default TableRow