import './styles.css';
import { useContext, useEffect,useState } from 'react';
import { CharsStats} from './../../context'
import TableRow from './../TableRow';

/* Odbieranie dzieci komponentu User*/


const Common = (props) => {
    const { name, str, hp, speed,addStr,addName,dmg,lvl,type,changeType } = useContext(CharsStats);
    const myStats={name,str,hp,speed,dmg,lvl};

//    console.log('type Common',type);

    useEffect(()=>{
        let nam;
        type==='User' ? nam=localStorage.getItem('name'): nam='Oponent'
//        console.log('type useFeect: ',type);
        addName(nam);
    },[type])
    

    return (
        <div className="col-6">
            <TableRow {...myStats}/>
 
        </div>
    )


}


export default Common;