
import './App.css';
import React from 'react';
import { GameApp } from './context';
import Homepage from './pages/homepage';

export function App() {


  //localStorage.clear();
  return (
    <div >
      <GameApp>
        <Homepage />
      </GameApp>
    </div>
  );
}

//export default { App };
