import React from 'react';
//import Header from '../../components/Header';
import User from '../../components/User';
import Klasa from '../../components/Klasa';


const Homepage = ({ title }) => {

    const name=localStorage.getItem('name');


    /*
    Zamiast paragrafu z Klasa:Bard mozemy użyć komponentu Klasa jak niżej
     */
    return (
        <div>
            <User poziom="10">
                {/* <p>Klasa: Bard</p> */}
                <Klasa nazwa="Bard" />
{/* wykorzystanie local storage 
                <p>Nick: {name}</p>
                */}
                <p>Nick: Demon</p>
                <p>Sila:10</p>
               <p>HP:100</p>
                 <p>Prędkość ataku:1</p>
            </User>
            {/* narazie jako przeciwnika dodajemy tego samego Usera */}
            <User poziom="10">
                {/* <p>Klasa: Bard</p> */}
                <Klasa nazwa="Bard" />
                <p>Nick: Demon2</p>
                <p>Sila:1</p>
                <p>HP:20</p>
                <p>Prędkość ataku:3</p>
           </User>
        </div>
    );

    /*
    Zamiast paragrafu z Klasa:Bard mozemy użyć komponentu Klasa jak niżej
     */



}

export default Homepage;