import './styles.css';
import React, { useContext, useEffect, useState } from 'react';
import Common from './../../components/Common';
//na początku importujemy UserStatsProvider
import { UserStatsProvider, OponentStatsProvider, CharsStats, GameState,  CommonStatsProvider } from './../../context';

import Form from './../../components/Form';

const Homepage = () => {

    //możemy wyświetlić parametry z CharsStats
    const { name, str, hp, speed, addStr, addName,type,changeType } = useContext(CharsStats);
    const { isName, setName } = useContext(GameState);

    useEffect(() => {
//        console.log('isName homepage: ', isName)
            const getName = localStorage.getItem('name');
            if (getName != null) {
                if (getName.length > 0) { setName(true) }
            };
    }, [isName])

    return (
        <div>
            {isName ? (
                <div className="game__wrapper">
                    <CommonStatsProvider nazwa='User'>
                        <Common />
                    </CommonStatsProvider>

                    <CommonStatsProvider nazwa='Oponent'  >
                        <Common alive />
                    </CommonStatsProvider>

                </div>
            ) : (
                <Form />
            )}
        </div>
    );
    // jeśli nie stworzonego providera,(np. dla drugiego Usera), to pobieran sa domyslne wartości z 
    // charsStat. Wartości w CharsStat bedą się zmieniać. UserStatsProvider bedzie miał inne, a OponentStatsProvide
    // będzie mial inne jeśli owprapowaliśmy w provider, to z niego 
    //będą pobierane wartośći.




}

export default Homepage;





